module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    testRegex: ['/src/e2e/'],
};
