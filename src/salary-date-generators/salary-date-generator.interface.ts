export interface SalaryDate {
    date: Date;
    type: 'base' | 'bonus';
}

export default interface ISalaryDateGenerator {
    generate(from: Date): Array<SalaryDate>;
}
