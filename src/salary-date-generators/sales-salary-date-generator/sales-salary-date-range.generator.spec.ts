import { mocked } from 'ts-jest/utils';
import SalesSalaryDateGenerator from './sales-salary-date-generator';
import SalesSalaryDateRangeGenerator from './sales-salary-date-range-generator';

const generateBaseDateMock = jest.fn().mockImplementation(() => {
    return new Date();
});

const generateBonusDateMock = jest.fn().mockImplementation(() => {
    return new Date();
});

jest.mock('./sales-salary-date-generator', () => {
    return {
        __esModule: true,
        default: jest.fn().mockImplementation(() => {
            return {
                generateBaseDate: generateBaseDateMock,
                generateBonusDate: generateBonusDateMock,
            };
        }),
    };
});

describe('SalesSalaryDateRangeGenerator', () => {
    const MockedSalesSalaryDateGenerator = mocked(
        SalesSalaryDateGenerator,
        true,
    );
    beforeEach(() => {
        MockedSalesSalaryDateGenerator.mockClear();
    });
    it('should generate dates for following 12 months including month form param', () => {
        const dateRangeGenerator = new SalesSalaryDateRangeGenerator();

        dateRangeGenerator.generate(new Date(1, 1, 2020));
        expect(generateBaseDateMock).toHaveBeenCalledTimes(12);
        expect(generateBonusDateMock).toHaveBeenCalledTimes(12);
    });
});
