import moment from 'moment';

export default class SalesSalaryDateGenerator {
    generateBaseDate(monthNumber: number, year: number): Date {
        const baseDate = new Date(Date.UTC(year, monthNumber, 0));

        if (this.isSunday(baseDate)) {
            return moment(baseDate).subtract(2, 'day').toDate();
        } else if (this.isSaturday(baseDate)) {
            return moment(baseDate).subtract(1, 'day').toDate();
        }

        return baseDate;
    }

    generateBonusDate(monthNumber: number, year: number): Date {
        const bonusDate = new Date(Date.UTC(year, monthNumber - 1, 15));

        if (this.isSunday(bonusDate)) {
            return moment(bonusDate).add(3, 'day').toDate();
        }
        if (this.isSaturday(bonusDate)) {
            return moment(bonusDate).add(4, 'day').toDate();
        }

        return bonusDate;
    }

    private isSunday(date: Date) {
        return date.getDay() === 0;
    }

    private isSaturday(date: Date) {
        return date.getDay() === 6;
    }
}
