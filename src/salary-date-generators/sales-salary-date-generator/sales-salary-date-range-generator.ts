import moment from 'moment';
import DateValidator from '../../validators/date-validator';
import ISalaryDateGenerator, {
    SalaryDate,
} from '../salary-date-generator.interface';
import SalesSalaryDateGenerator from './sales-salary-date-generator';

export default class SalesSalaryDateRangeGenerator
    implements ISalaryDateGenerator
{
    private dateGenerator: SalesSalaryDateGenerator;
    private dateValidator: DateValidator;

    constructor() {
        this.dateGenerator = new SalesSalaryDateGenerator();
        this.dateValidator = new DateValidator();
    }

    generate(from: Date): Array<SalaryDate> {
        const startDate = moment(
            new Date(from.getFullYear(), from.getMonth(), 1),
        );
        const endDate = startDate.clone().add(12, 'month');
        const salaryDates: Array<SalaryDate> = [];

        while (startDate.isBefore(endDate)) {
            const baseDate = this.dateGenerator.generateBaseDate(
                startDate.month() + 1,
                startDate.year(),
            );
            const bonusDate = this.dateGenerator.generateBonusDate(
                startDate.month() + 1,
                startDate.year(),
            );
            if (this.dateValidator.isGreaterThan(bonusDate, from)) {
                salaryDates.push({ date: bonusDate, type: 'bonus' });
            }

            if (this.dateValidator.isGreaterThan(baseDate, from)) {
                salaryDates.push({ date: baseDate, type: 'base' });
            }

            startDate.add(1, 'month');
        }

        return salaryDates;
    }
}
