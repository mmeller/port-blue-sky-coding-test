import { dateFromDateString } from '../../test_utils/test-utils';
import SalesSalaryDateGenerator from './sales-salary-date-generator';

describe('SalesSalaryDateRangeGenerator', () => {
    let salesSalaryDateRangeGenerator: SalesSalaryDateGenerator;

    beforeEach(() => {
        salesSalaryDateRangeGenerator = new SalesSalaryDateGenerator();
    });

    describe('generateBaseDate', () => {
        it('should return 31-03-2021 for 03-2021', () => {
            checkDateGeneration('31-03-2021', { month: 3, year: 2021 });
        });

        it('should return 26-02-2021 for 02-2021', () => {
            checkDateGeneration('26-02-2021', { month: 2, year: 2021 });
        });

        it('should return 30-07-2021 for 07-2021', () => {
            checkDateGeneration('30-07-2021', { month: 7, year: 2021 });
        });

        function checkDateGeneration(
            expectedDateString: string,
            checked: { month: number; year: number },
        ) {
            const actualDate = salesSalaryDateRangeGenerator.generateBaseDate(
                checked.month,
                checked.year,
            );
            expect(actualDate).toEqual(dateFromDateString(expectedDateString));
        }
    });

    describe('generateBonusDate', () => {
        it('should return 15-04-2021 for 04-2021', () => {
            checkDateGeneration('15-04-2021', { month: 4, year: 2021 });
        });

        it('should return 18-08-2021 for 08-2021', () => {
            checkDateGeneration('18-08-2021', { month: 8, year: 2021 });
        });

        it('should return 19-05-2021 for 05-2021', () => {
            checkDateGeneration('19-05-2021', { month: 5, year: 2021 });
        });

        function checkDateGeneration(
            expectedDateString: string,
            checked: { month: number; year: number },
        ) {
            const actualDate = salesSalaryDateRangeGenerator.generateBonusDate(
                checked.month,
                checked.year,
            );
            expect(actualDate).toEqual(dateFromDateString(expectedDateString));
        }
    });
});
