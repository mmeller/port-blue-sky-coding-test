import { Parser } from 'json2csv';
export class CsvFormatter<T> {
    private parser: Parser<T>;

    constructor() {
        this.parser = new Parser();
    }

    formatJsonToCsv(json: T): string {
        return this.parser.parse(json);
    }
}
