import { Application } from 'express';
import Server from '../server';
import supertest from 'supertest';

describe('E2E Server tests', () => {
    let app: Application;
    beforeEach(() => {
        const server = new Server();
        app = server.getExpressApp();
    });

    it('should return Error 400 for null query string', (done) => {
        supertest(app).get('/api/salary/sales').expect(400, done);
    });

    it('should return Error 400 for invalid date query string', (done) => {
        supertest(app)
            .get('/api/salary/sales?from=01-01-202a')
            .expect(400, done);
    });

    it('should return 200 for valid date query string with proper dates', (done) => {
        supertest(app)
            .get('/api/salary/sales?from=01-01-2021')
            .expect((res) => {
                expect(res.text.replace(/\s+/g, '')).toEqual(
                    `"date","type""2021-01-15T00:00:00.000Z","bonus""2021-01-29T00:00:00.000Z","base""2021-02-15T00:00:00.000Z","bonus""2021-02-26T00:00:00.000Z","base""2021-03-15T00:00:00.000Z","bonus""2021-03-31T00:00:00.000Z","base""2021-04-15T00:00:00.000Z","bonus""2021-04-30T00:00:00.000Z","base""2021-05-19T00:00:00.000Z","bonus""2021-05-31T00:00:00.000Z","base""2021-06-15T00:00:00.000Z","bonus""2021-06-30T00:00:00.000Z","base""2021-07-15T00:00:00.000Z","bonus""2021-07-30T00:00:00.000Z","base""2021-08-18T00:00:00.000Z","bonus""2021-08-31T00:00:00.000Z","base""2021-09-15T00:00:00.000Z","bonus""2021-09-30T00:00:00.000Z","base""2021-10-15T00:00:00.000Z","bonus""2021-10-29T00:00:00.000Z","base""2021-11-15T00:00:00.000Z","bonus""2021-11-30T00:00:00.000Z","base""2021-12-15T00:00:00.000Z","bonus""2021-12-31T00:00:00.000Z","base"`,
                );
            })
            .expect(200, done);
    });

    it('should return 200 for valid date query string with proper dates but exclude dates before FROM date', (done) => {
        supertest(app)
            .get('/api/salary/sales?from=16-01-2021')
            .expect((res) => {
                expect(res.text.replace(/\s+/g, '')).toEqual(
                    `"date","type""2021-01-29T00:00:00.000Z","base""2021-02-15T00:00:00.000Z","bonus""2021-02-26T00:00:00.000Z","base""2021-03-15T00:00:00.000Z","bonus""2021-03-31T00:00:00.000Z","base""2021-04-15T00:00:00.000Z","bonus""2021-04-30T00:00:00.000Z","base""2021-05-19T00:00:00.000Z","bonus""2021-05-31T00:00:00.000Z","base""2021-06-15T00:00:00.000Z","bonus""2021-06-30T00:00:00.000Z","base""2021-07-15T00:00:00.000Z","bonus""2021-07-30T00:00:00.000Z","base""2021-08-18T00:00:00.000Z","bonus""2021-08-31T00:00:00.000Z","base""2021-09-15T00:00:00.000Z","bonus""2021-09-30T00:00:00.000Z","base""2021-10-15T00:00:00.000Z","bonus""2021-10-29T00:00:00.000Z","base""2021-11-15T00:00:00.000Z","bonus""2021-11-30T00:00:00.000Z","base""2021-12-15T00:00:00.000Z","bonus""2021-12-31T00:00:00.000Z","base"`,
                );
            })
            .expect(200, done);
    });
});
