export default class DateValidator {
    validate(dateString?: string): boolean {
        if (dateString) {
            const dateParts = dateString.split('-');
            if (dateParts.length === 3) {
                const parsedDate = new Date(
                    Number(dateParts[2]),
                    Number(dateParts[1]) - 1,
                    Number(dateParts[0]),
                );
                return parsedDate.toString() !== 'Invalid Date';
            }
        }

        return false;
    }

    isGreaterThan(first: Date, second: Date): boolean {
        return first.getTime() > second.getTime();
    }
}
