import DateValidator from './date-validator';

describe('Date Validator tests', () => {
    let validator: DateValidator;

    beforeEach(() => {
        validator = new DateValidator();
    });

    it('should return true for valid date', () => {
        expect(validator.validate('01-01-2020')).toBeTruthy();
    });

    it('should return false if param is not a date', () => {
        expect(validator.validate('aaa')).toBeFalsy();
    });

    it('should return false for undefined param', () => {
        expect(validator.validate()).toBeFalsy();
    });
});
