import express, { Application } from 'express';
import moment from 'moment';
import { CsvFormatter } from './formatters/csv-formatter';
import ISalaryDateGenerator, {
    SalaryDate,
} from './salary-date-generators/salary-date-generator.interface';
import SalesSalaryDateRangeGenerator from './salary-date-generators/sales-salary-date-generator/sales-salary-date-range-generator';
import DateValidator from './validators/date-validator';

export default class Server {
    private application: Application;
    private dateValidator: DateValidator;
    private salesSalaryDateGenerator: ISalaryDateGenerator;
    private csvFormatter: CsvFormatter<Array<SalaryDate>>;

    constructor() {
        this.application = express();
        this.dateValidator = new DateValidator();
        this.salesSalaryDateGenerator = new SalesSalaryDateRangeGenerator();
        this.csvFormatter = new CsvFormatter();

        this.initRoutes();
    }

    public run(host: string, port: number): void {
        this.application.listen(port, host, () => {
            console.log(`Server is listening on HOST = ${host} PORT = ${port}`);
        });
    }

    public getExpressApp(): Application {
        return this.application;
    }

    private initRoutes() {
        this.application.get<unknown, unknown, unknown, { from: string }>(
            '/api/salary/sales',
            (req, res) => {
                if (this.dateValidator.validate(req.query.from)) {
                    const dates = this.salesSalaryDateGenerator.generate(
                        moment(req.query.from, 'DD-MM-YYYY').toDate(),
                    );
                    const csvDates = this.csvFormatter.formatJsonToCsv(dates);
                    res.attachment('salaries.csv').status(200).send(csvDates);
                } else {
                    if (req.query.from) {
                        res.status(400).send('Wrong date format');
                    } else {
                        res.status(400).send('Missing from parameter');
                    }
                }
            },
        );
    }
}
