import moment from 'moment';

export function dateFromDateString(dateString: string): Date {
    return moment.utc(dateString, 'DD-MM-YYYY').toDate();
}
