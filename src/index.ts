import Server from './server';
import Config from './config/config.json';

const server = new Server();

server.run(Config.host, Config.port);
