const path = require('path');
const config = require('./src/config/config.dev.json');

module.exports = {
    target: 'node',
    mode: 'development',
    entry: path.join(__dirname, 'src', 'index.ts'),
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js',
    },
    module: {
        rules: [
            {
                test: /.ts/,
                loader: 'ts-loader',
                include: path.resolve(__dirname, 'src'),
                exclude: path.resolve(__dirname, 'node_modules'),
            },
        ],
    },
    resolve: {
        extensions: ['.ts', '.js'],
    },
    externals: {
        './config/config.json': JSON.stringify(config),
    },
    devtool: 'source-map',
};
